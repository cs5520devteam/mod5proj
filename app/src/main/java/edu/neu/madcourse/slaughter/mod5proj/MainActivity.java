package edu.neu.madcourse.slaughter.mod5proj;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "MainActivity";

    boolean mTimerGoing = true;

    Timer mTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void mainStartTimer(View view){
 /*       TextView timerTV = (TextView)findViewById(R.id.timerTextView);

        int time = 0;

        mTimerGoing = true;
        while (mTimerGoing){
            time++;

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            timerTV.setText(Integer.toString(time));
            Log.d(TAG, "time: " + time);


            *//*
            for (int i = 0; i<100000; i++){
                do something:
            }*//*

        }*/

       mTimer = new Timer();

        mTimer.execute();
    }

    public void mainStopTimer(View view){

        /*mTimerGoing = false;*/
        mTimer.cancel(true);

    }


    public void mainStartService(View view){
        Log.d(TAG, "Starting service from MainActivity");
        Intent intent = new Intent(this, MyService.class);
        intent.putExtra("KEY1", "Value to be used by the service");
        intent.addCategory(CAMERA_SERVICE);

        startService(intent);
    }

    public void mainStopService(View view){
        Log.d(TAG, "Stop service from MainActivity");
        Intent intent = new Intent(this, MyService.class);
        intent.putExtra("KEY1", "Value to be used by the service");

        stopService(intent);
    }

    public void mainLaunchIntent(View view){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        //intent.setAction(Intent.CATEGORY_APP_CALENDAR);
        //intent.addCategory(Intent.CATEGORY_APP_EMAIL);
       // intent.setAction();
        //startActivity(intent);


        String uriText = "mailto:" + Uri.encode("email@gmail.com") +
                "?subject=" + Uri.encode("the subject") +
                "&body=" + Uri.encode("the body of the message");
        Uri uri = Uri.parse(uriText);

        intent.setData(uri);

        startActivity(intent);


/*
        Intent chooser = Intent.createChooser(intent, "Title here");

        // Verify the original intent will resolve to at least one activity
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(chooser);
        }
*/

    }




    private class Timer  extends AsyncTask<Void, Integer, Void> {



        @Override
        protected void onProgressUpdate(Integer... values) {
            TextView timerTV = (TextView)findViewById(R.id.timerTextView);
            timerTV.setText(Integer.toString(values[0]));

        }


        @Override
        protected Void doInBackground(Void... params) {
            for (int i=0; i<15; i++){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                publishProgress(i);
            }

            return null;
        }

    }

}
