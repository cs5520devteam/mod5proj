package edu.neu.madcourse.slaughter.mod5proj;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MyService extends Service {

    private static String TAG = "MyService";


    public MyService() {
    }

    @Override
    public void onCreate() {
        Log.v(TAG, "onCreate");
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.v(TAG, "onDestroy       ");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG, "onBind");
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG, "onStartCommand");

        Toast.makeText(this, " Service Started", Toast.LENGTH_LONG).show();

        // Return one of:
//        Service.START_STICKY
//        Service.START_NOT_STICKY
//        Service.START_REDELIVER_INTENT

        for (int i=0; i<15; i++){
            Log.v(TAG, "Running: " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return Service.START_STICKY;
        //return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean stopService(Intent name) {
        Log.v(TAG, "mainStopService");
        return super.stopService(name);
    }


    /***
     * Use bindService to start the service when you want to communicate with the service while it runs.
     * @param service
     * @param conn
     * @param flags
     * @return
     */
    @Override
    public boolean bindService(Intent service, ServiceConnection conn, int flags) {
        Log.v(TAG, "bindService");
        return super.bindService(service, conn, flags);
    }
}
